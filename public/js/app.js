var app = angular.module('app', ['ui.bootstrap'])

app.controller('Ctrl1', [ '$interval', '$scope', '$rootScope', function($interval, $scope, $rootScope) {

	var ctrl = this
    ctrl.count = 0
    
    $interval(function() {
        $rootScope.$broadcast('event1', { count: ctrl.count })
        ctrl.count++
        if(ctrl.count > 100) ctrl.count = 0
    }, 1000)

    $scope.$on('event1', function(event, arg) {
        console.log('Ctrl1 got event', event, arg)
    })

}])

app.controller('Ctrl2', [ '$scope', function($scope) {

    var ctrl = this
    ctrl.value = 0
	
    $scope.$on('event1', function(event, arg) {
        console.log('Ctrl2 got event1', event, arg)
        ctrl.value = arg.count
    })

}])